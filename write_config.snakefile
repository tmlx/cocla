#to run: snakemake -s ~/workbench/cocla2/cocla_v2.snakefile --configfile config.yaml --reason --cluster "sbatch -p pgsb -N {resources.nodes} -n {threads} -J {params.job_name} -e {params.loge} -o {params.logo} --mem {resources.MB} --exclude="kodiac,bombus,orca,barracuda"" --jobs 100 --resources load=100 --restart-times 1 --latency-wait 120 --max-jobs-per-second 7 --max-status-checks-per-second 7
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import os,re,sys,glob
from itertools import chain, islice

#####################################################################################################################################
# define variables


WRK_DIR = os.getcwd()

SP = ['amagalon','aslak','atlantica','bannister','bilby','bingo','clintland60','delfin','eriantha','fatua','FM13','gehl','GMI423','GS7','hatives','hifi','insularis','lion','longiglumis','nicolas','OT3098','OT380','park','PI182478','PI258586','PI388828','rhapsody','sang','TN1','TN4','victoria']


#####################################################################################################################################
# one rule to rule them all
rule all:
    input:
        expand("{sp}/cocla/config.cocla.yaml", sp = SP)




rule write_config:
    output:
        yaml = "{sp}/cocla/config.cocla.yaml",
    params:
        memory = "1G",
        job_name = "write_yaml",
        loge = WRK_DIR + "/write_yaml.log.e",
        logo = WRK_DIR + "/write_yaml.log.o",
    threads: 1
    resources:
        MB = 2000,
        nodes = 1,
        load = 1,
        threads = 1,
    run:
        with open(output.yaml,"w") as out:
            print("params:",file = out)
            print("  version: \"P80M80R80\" # or V1",file = out)
            print("  unipoa_threshold: 0.80 # complete",file = out)
            print("  unimag_threshold: 0.80 # reviewed",file = out)
            print("  repeat_threshold: 0.80 # trep",file = out)
            print("data:",file = out)
            print("    input:",file = out)
            print("      gff: ../{0}.gff3".format(wildcards.sp),file = out)
            print("      aa: ../{0}.aa.fa".format(wildcards.sp),file = out)
            print("      cds: ../{0}.cds.fa".format(wildcards.sp),file = out)
            print("      genome: ../{0}.combined.fa".format(wildcards.sp),file = out)
            print("      prefix: {0} # short sepcies name or nickname".format(wildcards.sp),file = out)
            print("    cocla:",file = out)
            print("        unimag: /nfs/pgsb/commons/databases/plant.annot/Classification/uniprot_Magnoliophyta_reviewed_collapsed_170220.fasta",file = out)
            print("        unipoa: /nfs/pgsb/commons/databases/plant.annot/Classification/uniprot_Poaceae_complete_collapsed_170220.fasta",file = out)
            print("        trep: /nfs/pgsb/commons/databases/plant.annot/Classification/trep-db_proteins_Rel-19.fasta",file = out)
            print("cocla:",file = out)
            print("    nbatches: 1000",file = out)
            print("    memory: 1G",file = out)
            print("    nodes: 1",file = out)
            print("    evalue: 10",file = out)
            print("executables:",file = out)
            print("    blastp: blastp",file = out)
            print("    gffread: gffread",file = out)
            
#             print("proteins_fasta: tmp/fasta.{0:05d}.fa".format(int(wildcards.i)),file = out)
#             print("blast_dbs:",file = out)
#             print("  sprot:",file = out)
#             print("    weight: {0}".format(config['databases']['sprot']['weight']),file = out)
#             print("    file: blast_sprot_out/blast.{0:05d}.sprot.blp".format(int(wildcards.i)),file = out)
#             print("    database: {0}".format(config['databases']['sprot']['db']),file = out)
#             print("    blacklist: {0}".format(config['data']['blacklist']),file = out)
#             print("    filter: {0}".format(config['databases']['sprot']['filter']),file = out)
#             print("    token_blacklist: {0}".format(config['data']['token_blacklist']),file = out)
#             print("    description_score_bit_score_weight: {0}".format(config['databases']['sprot']['dsbs_weight']),file = out)
#             print("  tair:",file = out)
#             print("    weight: {0}".format(config['databases']['tair']['weight']),file = out)
#             print("    file: blast_tair_out/blast.{0:05d}.tair.blp".format(int(wildcards.i)),file = out)
#             print("    database: {0}".format(config['databases']['tair']['db']),file = out)
#             print("    blacklist: {0}".format(config['data']['blacklist']),file = out)
#             print("    filter: {0}".format(config['databases']['tair']['filter']),file = out)
#             print("    token_blacklist: {0}".format(config['data']['token_blacklist']),file = out)
#             print("    description_score_bit_score_weight: {0}".format(config['databases']['tair']['dsbs_weight']),file = out)
#             print("    fasta_header_regex: ^>(?<accession>\S+)\s+(?<description>.+?)$",file = out)
#             print("    short_accession_regex: ^(?<shortAccession>.+)$",file = out)
#             print("  trembl_plants:",file = out)
#             print("    weight: {0}".format(config['databases']['trembl_plants']['weight']),file = out)
#             print("    file: blast_trembl_out/blast.{0:05d}.trembl.blp".format(int(wildcards.i)),file = out)
#             print("    database: {0}".format(config['databases']['trembl_plants']['db']),file = out)
#             print("    blacklist: {0}".format(config['data']['blacklist']),file = out)
#             print("    filter: {0}".format(config['databases']['trembl_plants']['filter']),file = out)
#             print("    token_blacklist: {0}".format(config['data']['token_blacklist']),file = out)
#             print("    description_score_bit_score_weight: {0}".format(config['databases']['trembl_plants']['dsbs_weight']),file = out)
#             print("interpro_database: {0}".format(config['databases']['ipscan']['db']),file = out)
#             print("interpro_result: interpro_out/interpro.{0:05d}.out".format(int(wildcards.i)),file = out)
#             print("gene_ontology_result: {0}".format(config['data']['gaf']),file = out)
#             print("token_score_bit_score_weight: 0.5",file = out)
#             print("token_score_database_score_weight: 0.3",file = out)
#             print("token_score_overlap_score_weight: 0.2",file = out)
#             print("output: ahrd_out/{0:05d}.ahrd.csv".format(int(wildcards.i)),file = out)
#             print("write_best_blast_hits_to_output: true",file = out)
#             print("write_scores_to_output: true",file = out)
