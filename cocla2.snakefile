#run on cluster with: snakemake -s cocla.snakefile final_files --cluster "qsub -N {params.job_name} -V -S /bin/bash -e {params.loge} -o {params.logo} -q plant2,plant_wheat -pe serial {threads} -l job_mem={params.memory}" --jobs 500 --latency-wait 300
#submit to slurm with: snakemake -s cocla.snakefile final_files --cluster "sbatch -p pgsb -n {threads} -J {params.job_name} -e {params.loge} -o {params.logo}" --jobs 100 --restart-times 1 --latency-wait 120

# configfile: "config.cocla2.yaml"

import csv,sys,re
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from itertools import chain, islice

def chunks(iterable, size=10):
    iterator = iter(iterable)
    for first in iterator:
        yield chain([first], islice(iterator, size - 1))

def aggregate_unimag(wildcards):
    checkpoint_output = checkpoints.split_fasta.get(**wildcards).output[0]
    file_names = expand("blastout/unimag/fasta.{i}.blp",i = glob_wildcards(os.path.join(checkpoint_output, "fasta.{i}.fa")).i)
    return file_names

def aggregate_unipoa(wildcards):
    checkpoint_output = checkpoints.split_fasta.get(**wildcards).output[0]
    file_names = expand("blastout/unipoa/fasta.{i}.blp",i = glob_wildcards(os.path.join(checkpoint_output, "fasta.{i}.fa")).i)
    return file_names

def aggregate_trep(wildcards):
    checkpoint_output = checkpoints.split_fasta.get(**wildcards).output[0]
    file_names = expand("blastout/trep/fasta.{i}.blp",i = glob_wildcards(os.path.join(checkpoint_output, "fasta.{i}.fa")).i)
    return file_names




INPUTGFF = config["data"]["input"]["gff"]
INPUTGENOME = config["data"]["input"]["genome"]
PREFIX = config["data"]["input"]["prefix"]
VERSION = config['params']['version']

WRK_DIR = os.getcwd()

#####################################################################################################################################
# confidence classification

rule all:
    input:
        PREFIX + ".cds.fa",
        PREFIX + ".aa.fa",
        "unimag.blastp.blp",
        "unipoa.blastp.blp",
        "trep.blastp.blp"
        

rule write_cds:
    input:
        gff3 = INPUTGFF,
        fasta = INPUTGENOME
    output:
        cds = PREFIX + ".cds.fa"
    params:
        job_name = "write_cds",
        logo = WRK_DIR + "/write_cds.log.o",
        loge = WRK_DIR + "/write_cds.log.e",
    threads: 1
    resources:
        MB = 4000,
        nodes = 1,
        load = 1,
        threads = 1
    message:
        ">>writing cds"
    shell:
        "gffread "
        "-g {input.fasta} "
        "{input.gff3} "
        "-x "
        "{output.cds}"

rule write_proteins:
    input:
        cds = PREFIX + ".cds.fa",
    output:
        prot = PREFIX + ".aa.fa"
    params:
        job_name = "write_prot",
        logo = WRK_DIR + "/write_prot.log.o",
        loge = WRK_DIR + "/write_prot.log.e",
    threads: 1
    resources:
        MB = 4000,
        nodes = 1,
        load = 1,
        threads = 1
    message:
        ">>writing protein"
    run:
        with open(output.prot, "w") as outfile:
            for record in SeqIO.parse(input.cds, "fasta"):
                if len(str(record.seq)) % 3 == 0:
                    tmprecord = SeqRecord(record.seq.translate(), id=record.id, description="")
                    SeqIO.write(tmprecord, outfile, "fasta")


checkpoint split_fasta:
    input:
        prot = PREFIX + ".aa.fa"
    output:
        outdir = directory("tmp/")
    params:
        job_name = "splitting",
        threads = 1,
        logo = WRK_DIR + "/splitting.log.o",
        loge = WRK_DIR + "/splitting.log.e",
        chunks = config['cocla']['chunks']
    threads: 1
    resources:
        MB = 2000,
        nodes = 1,
        Q = "pgsb",
        XTRA = "",
        load = 1,
        threads = 1
    message:
        "Splitting  proteins into smaller chunks"
#     log:
#         "{qry}.buildDb.log"
    run:
        shell("mkdir -p {output.outdir}")
        print(input.prot,params.chunks)
#         print(wildcards.qry)
        BATCHES = int((len(list(SeqIO.parse(input.prot,"fasta")))/params.chunks)+1)
#         print(BATCHES)
        sio = SeqIO.parse(input.prot, 'fasta')
        for cnt, chunk in enumerate(chunks(sio, params.chunks)):
            with open("tmp/fasta.{0:05d}.fa".format(cnt), 'w') as out:
                SeqIO.write(chunk, out, 'fasta')

rule blast_unimag:
    input:
        prot = "tmp/fasta.{i}.fa",
    output: "blastout/unimag/fasta.{i}.blp"
    params:
        job_name = "blasting",
        db = config["data"]["cocla"]["unimag"],
        loge = WRK_DIR + "/blasting.log.e",
        logo = WRK_DIR + "/blasting.log.o",
    threads: 1
    resources:
        MB = 8000,
        nodes = 1,
        Q = "pgsb",
        XTRA = config["Q"]["XTRA"],
        load = 1,
        threads = 1
    message:
        "Running blastp"
    shell:
        "diamond "
        "blastp "
        "--db {params.db} "
        "--more-sensitive "
        "--query {input.prot} "
        "--threads {threads} "
        "--out {output} "
        "--outfmt 6 "


rule blast_unipoa:
    input:
        prot = "tmp/fasta.{i}.fa",
    output: "blastout/unipoa/fasta.{i}.blp"
    params:
        job_name = "blasting",
        db = config["data"]["cocla"]["unipoa"],
        loge = WRK_DIR + "/blasting.log.e",
        logo = WRK_DIR + "/blasting.log.o",
    threads: 1
    resources:
        MB = 8000,
        nodes = 1,
        Q = "pgsb",
        XTRA = config["Q"]["XTRA"],
        load = 1,
        threads = 1
    message:
        "Running blastp"
    shell:
        "diamond "
        "blastp "
        "--db {params.db} "
        "--more-sensitive "
        "--query {input.prot} "
        "--threads {threads} "
        "--out {output} "
        "--outfmt 6 "

rule blast_trep:
    input:
        prot = "tmp/fasta.{i}.fa",
    output: "blastout/trep/fasta.{i}.blp"
    params:
        job_name = "blasting",
        db = config["data"]["cocla"]["trep"],
        loge = WRK_DIR + "/blasting.log.e",
        logo = WRK_DIR + "/blasting.log.o",
    threads: 1
    resources:
        MB = 8000,
        nodes = 1,
        Q = "pgsb",
        XTRA = config["Q"]["XTRA"],
        load = 1,
        threads = 1
    message:
        "Running blastp"
    shell:
        "diamond "
        "blastp "
        "--db {params.db} "
        "--more-sensitive "
        "--query {input.prot} "
        "--threads {threads} "
        "--out {output} "
        "--outfmt 6 "

rule merge_blasts:
    input:
        aggregate_unimag,
        aggregate_unipoa,
        aggregate_trep,
    output:
        unimag = "unimag.blastp.blp",
        unipoa = "unipoa.blastp.blp",
        trep = "trep.blastp.blp"
    params:
        job_name = "blast_merge",
        loge = WRK_DIR + "/blast_merge.log.e",
        logo = WRK_DIR + "/blast_merge.log.o",
    threads: 1
    resources:
        MB = 2000,
        load = 1,
        nodes = 1,
        Q = "pgsbshort",
        XTRA = "",
        threads = 1
    message:
        "Merging blastp outputs"
    run:
        shell("find blastout/trep -name \"*blp\" -exec cat {{}} \; "
        "> {output.trep}")
        shell("find blastout/unimag -name \"*blp\" -exec cat {{}} \; "
        "> {output.unimag}")
        shell("find blastout/unipoa -name \"*blp\" -exec cat {{}} \; "
        "> {output.unipoa}")



