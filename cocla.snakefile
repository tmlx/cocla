#run on cluster with: snakemake -s cocla.snakefile final_files --cluster "qsub -N {params.job_name} -V -S /bin/bash -e {params.loge} -o {params.logo} -q plant2,plant_wheat -pe serial {threads} -l job_mem={params.memory}" --jobs 500 --latency-wait 300
#submit to slurm with: snakemake -s cocla.snakefile final_files --cluster "sbatch -p pgsb -n {threads} -J {params.job_name} -e {params.loge} -o {params.logo}" --jobs 100 --restart-times 1 --latency-wait 120

configfile: "config.cocla.yaml"

import csv,os
from modules import fasta, mygff
from Bio import SeqIO
from Bio.Seq import Seq
#from Bio.Alphabet import IUPAC
from Bio.SeqRecord import SeqRecord


INPUTGFF = config["data"]["input"]["gff"]
INPUTAA = config["data"]["input"]["aa"]
INPUTCDS = config["data"]["input"]["cds"]
INPUTGENOME = config["data"]["input"]["genome"]
PREFIX = config["data"]["input"]["prefix"]
VERSION = config['params']['version']

WRK_DIR = os.getcwd()

#####################################################################################################################################
# confidence classification

rule all:
    input:
        genes = PREFIX+".genes_stats.tab",
        gff3 = PREFIX+"_"+VERSION+".transcripts.genes_cocla.gff3",

rule candidates_stats:
    input:
        gff3 = INPUTGFF,
        cds = INPUTCDS,
    output:
        genes = PREFIX+".genes_stats.tab",
        transcripts = PREFIX+".transcripts_stats.tab",
        cds = PREFIX+".cds_stats.tab"
    params:
        nodes = 1,
        memory = "16G",
        job_name = "stats",
        loge = WRK_DIR + "/cocla.log.e",
        logo = WRK_DIR + "/cocla.log.o",
    threads: 1
    run:
        print("Write CDS stats")
        fasta.PrintCdsStats(infasta=input.cds, outstats=output.cds)
        print("Read annotation")
        anno = mygff.GeneAnnotation().readGff3PlantAnnot(path=input.gff3)
        print("Write genes stats")
        anno.printGeneStats(path=output.genes)
        print("Write transcripts stats")
        anno.printTranscriptsStats(path=output.transcripts)


rule cocla_splitfasta:
    input:
        fasta=INPUTAA
    output:
        fastas=["cocla/{database}/batches/part_" + str(nbatch) + "/part_" + str(nbatch) + ".fasta"
            for nbatch in range(1, config["cocla"]["nbatches"]+1)]
    params:
        nodes = 1,
        memory = "8G",
        job_name = "cocla",
        loge = WRK_DIR + "/cocla.log.e",
        logo = WRK_DIR + "/cocla.log.o",
    threads: 1
    run:
        splitfasta = fasta.SplitSeqs(sequences=input.fasta, outdir="cocla/"+ wildcards.database +"/batches" , nfiles=config["cocla"]["nbatches"])

rule cocla_blast:
    input:
        fasta = "cocla/{database}/batches/part_{nbatch}/part_{nbatch}.fasta",
        database = lambda wildcards: config["data"]["cocla"][wildcards.database]
    output:
        blp="cocla/{database}/batches/part_{nbatch}/part_{nbatch}.blp"
    params:
        executable = config["executables"]["blastp"],
        nodes = config["cocla"]["nodes"],
        memory = config["cocla"]["memory"],
        evalue = config["cocla"]["evalue"],
        job_name = "cocla_blast",
        loge = WRK_DIR + "/cocla.log.e",
        logo = WRK_DIR + "/cocla.log.o",
    threads: 1
    run:
        shell(params.executable + " -evalue {params.evalue}  -num_threads {params.nodes} -subject {input.database} -query {input.fasta} -out {output.blp} -outfmt \"6 qseqid sseqid pident qlen qstart qend slen sstart send evalue bitscore\"")

rule cocla_blast_combine:
    input:
        blps=lambda wildcards: ["cocla/" + wildcards.database + "/batches/part_" + str(nbatch) + "/part_" + str(nbatch) + ".blp"
            for nbatch in range(1, config["cocla"]["nbatches"]+1)]
    output:
        blp="cocla/results_{database}.blp"
    params:
        nodes = 1,
        memory = "16G",
        job_name = "cocla",
        loge = WRK_DIR + "/cocla.log.e",
        logo = WRK_DIR + "/cocla.log.o",
    threads: 1
    run:
        shell("touch {output.blp}")
        for blp in input.blps:
             shell("cat " + blp + " >> {output.blp}")

rule cocla_all:
    input:
        transcripts = PREFIX+"_"+VERSION+".transcripts_stats.tab",
        cds = PREFIX+"_"+VERSION+".cds_stats.tab",
        trep = "cocla/results_trep.blp",
        unimag = "cocla/results_unimag.blp",
        unipoa = "cocla/results_unipoa.blp"

rule cocla_report:
    input:
        transcripts = PREFIX+".transcripts_stats.tab",
        cds = PREFIX+".cds_stats.tab",
        trep = "cocla/results_trep.blp",
        unimag = "cocla/results_unimag.blp",
        unipoa = "cocla/results_unipoa.blp"
    output:
        report = "confidence_classification_"+VERSION+".html"
    params:
        nodes = 1,
        memory = "16G",
        job_name = "cocla_report",
        loge = WRK_DIR + "/cocla.log.e",
        logo = WRK_DIR + "/cocla.log.o",
        version = VERSION,
        unipoa_threshold = config['params']['unipoa_threshold'],
        unimag_threshold = config['params']['unimag_threshold'],
        repeat_threshold = config['params']['repeat_threshold'],
    threads: 1
    script:
        "confidence_classification.Rmd"

#####################################################################################################################################
# get final files

rule final_files:
    input:
        gff3 = INPUTGFF,
        report = "confidence_classification_"+VERSION+".html"
    output:
        gff3 = PREFIX+"_"+VERSION+".transcripts.genes_cocla.gff3",
        gff3HC = PREFIX+"_"+VERSION+".transcripts.genes_cocla_HC.gff3",
        gff3LC = PREFIX+"_"+VERSION+".transcripts.genes_cocla_LC.gff3",
        cds = PREFIX+"_"+VERSION+".transcripts.genes_cocla_cds.fasta",
        cdsHC = PREFIX+"_"+VERSION+".transcripts.genes_cocla_HC_cds.fasta",
        cdsLC = PREFIX+"_"+VERSION+".transcripts.genes_cocla_LC_cds.fasta",
        protein = PREFIX+"_"+VERSION+".transcripts.genes_cocla_cds_proteins.fasta",
        proteinHC = PREFIX+"_"+VERSION+".transcripts.genes_cocla_HC_cds_proteins.fasta",
        proteinLC = PREFIX+"_"+VERSION+".transcripts.genes_cocla_LC_cds_proteins.fasta"
    params:
        executable = config["executables"]["gffread"],
        genome = INPUTGENOME,
        nodes = 1,
        memory = "16G",
        job_name = "finalising",
        loge = WRK_DIR + "/cocla.log.e",
        logo = WRK_DIR + "/cocla.log.o",
    threads: 1
    run:
        conflcass = {}
        with open("confidence_classification_"+VERSION+".tab", "r") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=",")
            for line in reader:
                conflcass[line["id"]] = {}
                conflcass[line["id"]]["primconf"] = line["primconf"]
                conflcass[line["id"]]["secconf"] = line["secconf"]
        anno = mygff.GeneAnnotation().readGff3PlantAnnot(path=input.gff3)
        newfeatures = []
        newgenes = {}
        newmrnas = {}
        newseqids = {}
        for geneid in anno.genes:
            gene = anno.genes[geneid]
            tmpclass = "none"
            tmphcmrnas = []
            tmplcmrnas = []
            for mrna in gene.features:
                mrna.primary_confidence_class = conflcass[mrna.identifier]["primconf"]
                mrna.secondary_condidence_class = conflcass[mrna.identifier]["secconf"]
                if conflcass[mrna.identifier]["primconf"] == "HC":
                    tmpclass = "HC"
                    tmphcmrnas.append(mrna)
                if tmpclass != "HC" and conflcass[mrna.identifier]["primconf"] == "LC":
                    tmpclass = "LC"
                    tmplcmrnas.append(mrna)
            if tmpclass=="HC":
                gene.features = tmphcmrnas
                gene.primary_confidence_class = "HC"
                newfeatures.append(gene)
                newgenes[gene.identifier] = gene
                for mrna in tmphcmrnas:
                    newfeatures.append(mrna)
                    newmrnas[mrna.identifier] = mrna
                    newfeatures += mrna.features
            if tmpclass=="LC":
                gene.mrnas = tmplcmrnas
                gene.primary_confidence_class = "LC"
                newfeatures.append(gene)
                newgenes[gene.identifier] = gene
                for mrna in tmplcmrnas:
                    newfeatures.append(mrna)
                    newmrnas[mrna.identifier] = mrna
                    newfeatures += mrna.features
        anno.features = sorted(newfeatures)
        anno.genes = newgenes
        anno.mrnas = newmrnas
        print("Write new gff3 files")
        anno_newgenes = anno.recalcGeneids()
        anno_newgenes.writeGff3Genes(output.gff3)
        anno_hc = anno.getHcGff3Genes()
        anno_hc.writeGff3Genes(output.gff3HC)
        anno_lc = anno.getLcGff3Genes()
        anno_lc.writeGff3Genes(output.gff3LC)
        print("Write coding sequences")
        shell("{params.executable} {output.gff3} -g {params.genome} -x {output.cds}")
        shell("{params.executable} {output.gff3HC} -g {params.genome} -x {output.cdsHC}")
        shell("{params.executable} {output.gff3LC} -g {params.genome} -x {output.cdsLC}")
        print("Write proteins sequences")
        with open(output.cds, "r") as infile:
            with open(output.protein, "w") as outfile:
                for record in SeqIO.parse(infile, "fasta"):
                    if len(str(record.seq)) % 3 == 0:
                        tmprecord = SeqRecord(record.seq.translate(), id=record.id, description="")
                        SeqIO.write(tmprecord, outfile, "fasta")
        with open(output.cdsHC, "r") as infile:
            with open(output.proteinHC, "w") as outfile:
                for record in SeqIO.parse(infile, "fasta"):
                    if len(str(record.seq)) % 3 == 0:
                        tmprecord = SeqRecord(record.seq.translate(), id=record.id, description="")
                        SeqIO.write(tmprecord, outfile, "fasta")
        with open(output.cdsLC, "r") as infile:
            with open(output.proteinLC, "w") as outfile:
                for record in SeqIO.parse(infile, "fasta"):
                    if len(str(record.seq)) % 3 == 0:
                        tmprecord = SeqRecord(record.seq.translate(), id=record.id, description="")
                        SeqIO.write(tmprecord, outfile, "fasta")
